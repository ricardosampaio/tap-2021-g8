# **Sumário executivo**

- Diogo Batista 1150802
- Ricardo Sampaio 1151680
- Miguel Moreira 1171280

## MS01
### Modelo de Domínio

É apresentado de seguida uma representação de **alto nível** das entidades de domínio.

![domain model diagram](domainModel.png)

* **Production** - representa a produção de uma tarefa completa de uma order e tem como atributo uma TaskSchedule.
* **TaskSchedule** - representa uma task agendada e tem como atributos a sua duração, a hora de início, uma order e um produto.
* **Order** -  representa uma order criada e tem como atributos um id, uma referência a um produto e a quantidade de produtos a serem produzidos.
* **Product** - representa um produto criado e tem como atributos um id (usado como referência em order), um nome e uma lista de tarefas a serem executadas, para a criação deste produto.
* **Task** - representa uma tarefa para a criação de um certo produto e tem como atributos um id, um tempo, uma lista de recursos físicos e uma lista de recursos humanos disponíveis, que são especializados nesses recursos físicos.
* **Resource** - representação abstrata de um recurso e tem como atributos um id e um tipo.
* **PhysicalResource** - representação concreta de um recurso físico e tem como atributos um id e um tipo.
* **HumanResource** - representação concreta de um recurso humano (especializado em um, ou vários recursos físicos) e tem como atributos um id e uma lista de recursos físicos em que é especializado.

### Tratamento de erros

Desde o início da execução do programa, onde a informação é lida dos ficheiros XML, são verificados os casos onde os dados de entrada encontram-se inválidos consoante as regras de domínio.

Portanto, o processo de construção é validado na íntegra desde atributos como, id seguir umas regras específicas, por exemplo Produto ter obrigatoriamente de ser iniciado com "PRD_" seguido de um número, datas de início não serem superiores ou iguais às de fim, etc.

No caso de existirem violações às regras de domínio, o programa imediatamente termina execução e lança uma mensagem de erro descritiva da localização e causa deste (encapsulado num tipo Either).

Foram criados types génericos como NonEmptyString, PositiveInteger ou Time que servem como tipos onde o seu conteúdo já se encontra devidamente validado. Estas classes são depois utilizadas para representar os atributos das entidades de negócio.

### Processo de escalonamento

Relativamente ao tratamento de intervalos, foi feito um esforço pelo grupo de modo a evitar a utilização de métodos recursivos, de tal modo que estes não existem no projeto, sendo apenas utilizados métodos já nativos do Scala. Isto apresenta uma grande vantagem dado que são métodos já devidamente testados, por oposição se fossem utilizados métodos recursivos existiria uma maior chance de erros.

O processo para escalonar uma prova encontra-se descrito abaixo, por ordem de execução:

* Ler todos os dados dos ficheiros de input (XML) para posterior agregação em case classes;
* Iterar sobre as orders, de forma a obter todos os produtos;
* Iterar sobre os produtos, de forma a obter todas as tasks;
* Ir a cada task e verificar que human resource está especializado nos physical resources necessários;
* Criar o Schedule

### Possíveis melhorias

Poderíamos ter classes criadas para albergar todo o tratamento de listas que existem, lista de orders, de humanResources, PhysicalResources, etc... Com este tipo de classes, o código duplicado era residual, tornando-se assim mais limpo e fiável de que o mesmo tipo de lista, executaria sob as mesmas condições
Falta-nos validações, em casos de erro, especialmente no lançamento de uma mensagem específica, aquando a existência de um erro

## MS02
### Modelo de Domínio

É apresentado de seguida uma representação de **alto nível** das entidades de domínio.

![domain model diagram](domainModel2.png)

* **ScheduledTask** - representa uma task agendada e tem como atributos a sua hora de fim, a hora de início, um id de uma order, um product number, os recursos físicos existentes e os recursos humans disponíveis.
* **Order** -  representa uma order criada e tem como atributos um id, uma referência a um produto e a quantidade de produtos a serem produzidos.
* **Product** - representa um produto criado e tem como atributos um id (usado como referência em order), um nome e uma lista de tarefas a serem executadas, para a criação deste produto.
* **Task** - representa uma tarefa para a criação de um certo produto e tem como atributos um id, um tempo, uma lista de recursos físicos e uma lista de recursos humanos disponíveis, que são especializados nesses recursos físicos.
* **PhysicalResourceType** - representação abstrata de um recurso e tem como atributos um resourceType, ou seja, um tipo de recurso
* **PhysicalResource** - representação concreta de um recurso físico e tem como atributos um id e um tipo.
* **HumanResource** - representação concreta de um recurso humano (especializado em um, ou vários recursos físicos) e tem como atributos um id e uma lista de recursos físicos em que é especializado.

### Generators
Para criarmos os PropertyBasedTests foi necessário a criação de alguns generators, de forma a preencher automaticamente e aleatoriamente (segundo algumas regras) todos os dados necessários para a execução do algoritmo.

Posto isto, foram criados diversos generators:

| Tipo   |      Generator      |  Descrição |
|----------|:-------------:|------:|
| Generic    |  genList  |  Para o genList é gerado uma lista com um tamanho de elementos compreendida entre um valor min e um valor max
| SimpleType |  genTime | Para o genTime é gerado um número aleatório entre 30 a 3600 |
| SimpleType |    genProductName   |   Para o genProductName é gerado um name aleatório com tamanho entre 5 a 12 caracteres |
| SimpleType | genHumanName |   Para o genHumanName é gerado um name aleatório com tamanho entre 5 a 12 caracteres |
| SimpleType | genOrderId |   Para o genOrderId é gerado um id que começa por "ORD_" seguido de um número aleatório de 1 a 9, sendo que pode ter 1 ou 2 algarismos |
| SimpleType | genProductId |   Para o genProductId é gerado um id que começa por "PRD_" seguido de um número aleatório de 1 a 9, sendo que pode ter 1 ou 2 algarismos  |
| SimpleType | genHumanId |   Para o genHumanId é gerado um id que começa por "HRS_" seguido de um número aleatório de 1 a 9, sendo que pode ter 1 ou 2 algarismos  |
| SimpleType | genTaskId |   Para o genTaskId é gerado um id que começa por "TSK_" seguido de um número aleatório de 1 a 9, sendo que pode ter 1 ou 2 algarismos  |
| SimpleType | genPhysicalId |   Para o genPhysicalId é gerado um id que começa por "PRS_" seguido de um número aleatório de 1 a 9, sendo que pode ter 1 ou 2 algarismos  |
| SimpleType | genResourceType |   Para o genResourceType é gerado um resourceType que começa por "PRST " seguido de um número aleatório de 1 a 9, sendo que pode ter 1 ou 2 algarismos  |
| SimpleType | genQuantity |   Para o genQuantity é gerado uma quantidade entre 1 a 3 |
| Physical | genPhysicalResource |   Para o genPhysicalResource é gerado um Physical que tem um id gerado pelo genPhysicalId e um resourceType que é escolhido aleatoriamente, a partir de uma lista que recebe |
| Order | genOrder |   Para o genOrder é gerado uma Order que tem um id gerado pelo genOrderId, um id de um produto que é escolhido aleatoriamente, a partir de uma lista que recebe e uma quantidade que é gerada através do genQuantity |
| Product | genProduct |   Para o genProduct é gerado um Product que tem um id gerado pelo genProductId, um name gerado pelo genProductName e uma processList, que contém pelo menos um elemento e com elementos aleatórios, a partir de uma lista que recebe |
| Process | genProcess |   Para o genProcess é gerado um Process que tem uma TaskId que é escolhida aleatoarimente, através de uma lista que recebe |
| Human | genHuman |   Para o genHuman é gerado um Human que tem um humanid gerado pelo genHumanId, um name gerado pelo genHumanName e um handles, que contém pelo menos um elemento e, no máximo 2, com elementos aleatórios, a partir de uma lista de Physical que recebe |
| Task | genTask |   Para o genTask é gerado uma Task que tem um taskId gerado pelo genTaskId, um time gerado pelo genTime e um resources, que contém pelo menos um elemento e, no máximo 2, com elementos aleatórios, a partir de uma lista de ResourceType que recebe |
| Production | genProduction |   Para o genProduction é gerado uma Production que tem 7 listas distintas, geradas através de todos os generators definidos anteriormente para a criação de Domain.classes |
| ResourceType | genResourceType | Para o genResourceType é gerado um PhysicalResourceType que é escolhido aleatoriamente, através de uma lista de Physical que recebe.

### Properties

#### Property 1 : The same resource cannot be used at the same time by two tasks
Utilizando a função validateTasks verificamos, se em algum momento, um Physical ou um Human está a ser utilizado ao mesmo tempo, por 2 tarefas distinas.

#### Property 2 : The complete Schedule must schedule all tasks of all products needed
A propriedade verifica se o ScheduledTask contém todas as Tasks de todos os Product a serem produzidos.

#### Property 3 : Must create unique Id's for all classes created [NÃO IMPLEMENTADO]
A propriedade verifica se todos os id's vão ser únicos, ou seja, se não existem ids repetidos.
Neste caso, a propriedade é verdadeira, visto que geramos o Production só com elementos únicos.

#### Property 4 : The total Production time must be the sum of all tasks scheduled [NÃO IMPLEMENTADO]
A propriedade verifica se a soma de todas as tasks equivalem ao endTime da última task executada.

### Possíveis melhorias
Devido a termos uma primeira parte do projeto (Milestone 1) muito defecitária, não foi possível executar tudo conforme o pedido. Posto isto, como possível melhorias temos algumas validações necessárias para a execução a 100% dos testes fornecidos para a validação do algoritmo.
Outra das melhorias necessárias, aliadas à falta de "tempo" para a correção e atualização do projeto, é a adição de Result[X]. Pois, com isto é mais fácil tratar os erros.
Poderíamos ter criado mais algumas properties
