package domain

import domain.DomainError.{InvalidTaskId, NegativeTime}
import domain.SimpleTypes.{ProductName, Quantity, TaskId, Time, ResourceType}
import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions

class TaskTests extends AnyFunSuite {
  test("Create Task - no errors") {
    val xml = scala.xml.XML.loadString({
      <Task id="TSK_1" time="100">
        <PhysicalResource type="PRST 1"/>
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 3"/>
      </Task>
    }.toString)

    val resourceType1 = ResourceType.from("PRST 1")
    val resourceType2 = ResourceType.from("PRST 2")
    val resourceType3 = ResourceType.from("PRST 3")

    for {
      tskid <- TaskId.from("TSK_1")
      tsktime <- Time.from(100)
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      r3 <- Physical.from(<Physical id="PRS_3" type="PRST 3"/>)
      result <- Task.from(List(r1,r2,r3),List[Human]())(xml)
    } yield assert(result.id === tskid && result.time === tsktime && result.resources.map(t => t.resourceType) === List(r1.resourceType, r2.resourceType, r3.resourceType))

    val result = for {
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      r3 <- Physical.from(<Physical id="PRS_3" type="PRST 3"/>)
      result <- Task.from(List(r1,r2,r3),List[Human]())(xml)
    } yield result
    assert(result.isLeft)
  }

  test("Create Task - TaskId error") {
    val xml = scala.xml.XML.loadString({
      <Task id="TSK19" time="100">
        <PhysicalResource type="PRST 1"/>
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 3"/>
      </Task>
    }.toString)

    val resourceType1 = ResourceType.from("PRST 1")
    val resourceType2 = ResourceType.from("PRST 2")
    val resourceType3 = ResourceType.from("PRST 3")

    for {
      tskid <- TaskId.from("TSK19")
      tsktime <- Time.from(100)
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      r3 <- Physical.from(<Physical id="PRS_3" type="PRST 3"/>)
      result <- Task.from(List(r1,r2,r3),List[Human]())(xml)
    } yield assert(result.id === tskid && result.time === tsktime && result.resources === List(r1, r2, r3))

    val result = for {
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      r3 <- Physical.from(<Physical id="PRS_3" type="PRST 3"/>)
      result <- Task.from(List(r1,r2,r3),List[Human]())(xml)
    } yield result
    assert(result.isLeft)
    assert(result.fold(error => error === InvalidTaskId("TSK19"), _ => false))
  }

  test("Create Task - Time error") {
    val xml = scala.xml.XML.loadString({
      <Task id="TSK_1" time="-100">
        <PhysicalResource type="PRST 1"/>
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 3"/>
      </Task>
    }.toString)

    val resourceType1 = ResourceType.from("PRST 1")
    val resourceType2 = ResourceType.from("PRST 2")
    val resourceType3 = ResourceType.from("PRST 3")

    for {
      tskid <- TaskId.from("TSK_1")
      tsktime <- Time.from(-100)
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      r3 <- Physical.from(<Physical id="PRS_3" type="PRST 3"/>)
      result <- Task.from(List(r1,r2,r3),List[Human]())(xml)
    } yield assert(result.id === tskid && result.time === tsktime && result.resources === List(r1, r2, r3))

    val result = for {
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      r3 <- Physical.from(<Physical id="PRS_3" type="PRST 3"/>)
      result <- Task.from(List(r1,r2,r3),List[Human]())(xml)
    } yield result
    assert(result.isLeft)
    assert(result.fold(error => error === NegativeTime(-100), _ => false))
  }
}