package domain

import domain.DomainError.*
import domain.SimpleTypes.*
import org.scalatest.funsuite.AnyFunSuite
import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll
import org.scalacheck.*
import domain.Order


import scala.language.adhocExtensions

class OrderTests extends AnyFunSuite {
  
  test("Create Order - No errors") {
    val xml = scala.xml.XML.loadString("<Order id=\"ORD_1\" prdref=\"PRD_1\" quantity=\"1\"/>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    val product1 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_1" name="Computador ASUS"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    val product2 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_2" name="Monitor Curvo AOC 27'"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    for {
      id <- OrderId.from("ORD_1")
      product <- ProductId.from("PRD_1")
      quantity <- Quantity.from("1")
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRD_1" quantity="1"/>)
    } yield assert(result.id === id && result.prdRef === product && result.quantity === quantity)

    val result = for {
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRD_1" quantity="1"/>)
    } yield result
    assert(result.isRight)
  }

  test("Create Order - OrderId errors") {
    val xml = scala.xml.XML.loadString("<Order id=\"OLAL_01\" prdref=\"PRD_1\" quantity=\"1\"/>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    val product1 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_1" name="Computador ASUS"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    val product2 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_2" name="Monitor Curvo AOC 27'"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    for {
      id <- OrderId.from("OLAL_01")
      product <- ProductId.from("PRD_1")
      quantity <- Quantity.from("1")
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRD_1" quantity="1"/>)
    } yield assert(result.id != id && result.prdRef === product && result.quantity === quantity)

    val result = for {
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="OLAL_01" prdref="PRD_1" quantity="1"/>)
    } yield result
    assert(result.fold(error => error == InvalidOrderId("OLAL_01"), _ => false))
  }

  test("Create Order - ProdId errors") {
    val xml = scala.xml.XML.loadString("<Order id=\"ORD_1\" prdref=\"PRODID_1\" quantity=\"1\"/>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    val product1 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_1" name="Computador ASUS"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    val product2 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_2" name="Monitor Curvo AOC 27'"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    for {
      id <- OrderId.from("ORD_1")
      product <- ProductId.from("PRD_1")
      quantity <- Quantity.from("2")
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRODID_1" quantity="1"/>)
    } yield assert(result.id === id && result.prdRef === product && result.quantity === quantity)

    val result = for {
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRODID_1" quantity="1"/>)
    } yield result
    assert(result.isLeft)
    assert(result.fold(error => error == InvalidProductId("PRODID_1"), _ => false))
  }

  test("Create Order - Quantity erros") {
    val xml = scala.xml.XML.loadString("<Order id=\"ORD_1\" prdref=\"PRD_1\" quantity=\"0\"/>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    val product1 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_1" name="Computador ASUS"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    val product2 = for {
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(<Product id="PRD_2" name="Monitor Curvo AOC 27'"><Process
        tskref="TSK_1"/><Process tskref="TSK_2"/></Product>)
    } yield result

    for {
      id <- OrderId.from("ORD_1")
      product <- ProductId.from("PRD_1")
      quantity <- Quantity.from("2")
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRD_1" quantity="0"/>)
    } yield assert(result.id === id && result.prdRef === product && result.quantity === quantity)

    val result = for {
      p1 <-  product1
      p2 <-  product2
      result <- Order.from(List(p1, p2))(<Order id="ORD_1" prdref="PRD_1" quantity="0"/>)
    } yield result
    assert(result.isLeft)
    assert(result.fold(error => error == InvalidQuantity(0), _ => false))
  }
}