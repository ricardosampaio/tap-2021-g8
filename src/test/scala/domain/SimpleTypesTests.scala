package domain

import domain.DomainError.*
import domain.SimpleTypes.*
import org.scalatest.funsuite.AnyFunSuite
import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll
import org.scalacheck.*
import scala.language.adhocExtensions

class SimpleTypesTests extends AnyFunSuite {

  test("Valid resourceType") {
    val resourceType = "PRST 1"
    val result = ResourceType.from(resourceType)

    assert(result.fold(er => false, m => m.toString == resourceType))
  }


  test("Invalid resourceType without numbers") {
    val resourceType = "PRST "
    val result = ResourceType.from(resourceType)

    assert(result.fold(er => er == InvalidResourceType(resourceType), m => false))
  }

  test("Invalid resourceType") {
    val resourceType = "PRS 2"
    val result = ResourceType.from(resourceType)

    assert(result.fold(er => er == InvalidResourceType(resourceType), m => false))
  }

  test("Valid order id") {
    val order = "ORD_100"
    val result = OrderId.from(order)

    assert(result.fold(er => false, m => m.toString == order))
  }


  test("Invalid order id") {
    val order = "OR_101"
    val result = OrderId.from(order)

    assert(result.fold(er => er == InvalidOrderId(order), m => false))
  }

  test("Valid quantity") {
    val qt = 14
    val result = Quantity.from(qt)

    assert(result.fold(_ => false, quantity => quantity.toString == qt.toString))
  }

  test("Invalid negative quantity") {
    val qt = -1
    val result = Quantity.from(qt)

    assert(result.fold(error => error == InvalidQuantity(qt), _ => false))
  }

  test("Invalid zero quantity") {
    val qt = 0
    val result = Quantity.from(qt)

    assert(result.fold(error => error == InvalidQuantity(qt), _ => false))
  }

  test("String quantity with number is valid") {
    val qt = 14
    val result = Quantity.from(qt.toString())

    assert(result.fold(_ => false, quantity => quantity.toString == qt.toString))
  }


  test("String quantity with letters is invalid") {
    val qt = "a"
    val result = Quantity.from(qt)

    assert(result.fold(error => error == FailedQuantityConversion(qt), _ => false))
  }

  test("Valid Product") {
    val prod = "PRD_14"
    val result = ProductId.from(prod)

    assert(result.fold(_ => false, productId => productId.toString == prod))
  }

  test("Invalid product") {
    val prod = "PR_14"
    val result = ProductId.from(prod)

    assert(result.fold(error => error == InvalidProductId(prod), _ => false))
  }

  test("Invalid empty product") {
    val prod = ""
    val result = ProductId.from(prod)

    assert(result.fold(error => error == InvalidProductId(prod), _ => false))
  }

  test("Valid Product Name") {
    val prodname = "Computador ASUS"
    val result = ProductName.from(prodname)

    assert(result.fold(_ => false, productName => productName.toString == prodname))
  }

  test("Invalid empty Product Name") {
    val prodname = ""
    val result = ProductName.from(prodname)

    assert(result.fold(error => error == InvalidProductName(prodname), _ => false))
  }

  /*Human Resources*/

  /*val genHumanId: Gen[HumanId] = for {
    n <- Gen.chooseNum(1, 5)
    listn <- Gen.listOfN(n, Gen.chooseNum(1,9))
    id <- HumanId.from("HRS_" + listn.mkString).fold(_ => Gen.fail, hid => Gen.const(hid))
  } yield id
*/
  test("Valid Human Resource") {
    val hrs = "HRS_1"
    val result = HumanId.from(hrs)

    assert(result.fold(_ => false, humanId => humanId.toString == hrs))
  }

  test("Invalid Human Resource 0") {
    val hrs = "HR_0"
    val result = HumanId.from(hrs)

    assert(result.fold(error => error == InvalidHumanId(hrs), _ => false))
  }

  test("Invalid Human Resource") {
    val hrs = "HR_12"
    val result = HumanId.from(hrs)

    assert(result.fold(error => error == InvalidHumanId(hrs), _ => false))
  }

  test("Invalid empty Human Resource") {
    val hrs = ""
    val result = HumanId.from(hrs)

    assert(result.fold(error => error == InvalidHumanId(hrs), _ => false))
  }

  test("Valid Human Resource name") {
    val hrsname = "Miguel"
    val result = HumanName.from(hrsname)

    assert(result.fold(_ => false, humanName => humanName.toString == hrsname))
  }

  test("Invalid empty Human Reourse name") {
    val hrsname = ""
    val result = HumanName.from(hrsname)

    assert(result.fold(error => error == InvalidHumanName(hrsname), _ => false))
  }

  test("Valid Task") {
    val tsk = "TSK_19"
    val result = TaskId.from(tsk)

    assert(result.fold(_ => false, v => v.toString == tsk))
  }

  test("Invalid Task") {
    val tsk = "TS_2020"
    val result = TaskId.from(tsk)

    assert(result.fold(error => error == InvalidTaskId(tsk), _ => false))
  }

  test("Invalid empty Task Id") {
    val tsk = ""
    val result = TaskId.from(tsk)

    assert(result.fold(error => error == InvalidTaskId(tsk), _ => false))
  }

  test("Valid PhysicalId") {
    val phId = "PRS_11"
    val result = PhysicalId.from(phId)

    assert(result.fold(_ => false, physicalId => physicalId.toString == phId))
  }

  test("Invalid PhysicalId") {
    val phId = "P_12"
    val result = PhysicalId.from(phId)

    assert(result.fold(error => error == InvalidPhysicalId(phId), _ => false))
  }

  test("Invalid PhysicalId 0") {
    val phId = "P_0"
    val result = PhysicalId.from(phId)

    assert(result.fold(error => error == InvalidPhysicalId(phId), _ => false))
  }

  test("Invalid empty PhysicalId") {
    val phId = ""
    val result = PhysicalId.from(phId)

    assert(result.fold(error => error == InvalidPhysicalId(phId), _ => false))
  }

  /* TaskSchedule */

  test("Valid ProductNumber") {
    val value = 147
    val result = ProductNumber.from(value)

    assert(result.fold(_ => false, productNumber => productNumber.toString == value.toString))
  }

  test("Invalid ProductNumber") {
    val value = -2
    val result = ProductNumber.from(value)

    assert(result.fold(error => error == InvalidProductNumber(value), _ => false))
  }

  test("Valid StartTime") {
    val stime = 10
    val result = StartTime.from(stime)

    assert(result.fold(_ => false, startTime => startTime.toString == stime.toString))
  }

  test("Invalid negative StartTime") {
    val stime = -10
    val result = StartTime.from(stime)

    assert(result.fold(error => error == InvalidStartTime(stime), _ => false))
  }

  test("Valid 0 StartTime") {
    val stime = 0
    val result = StartTime.from(stime)

    assert(result.fold(_ => false, startTime => startTime.toString == stime.toString))
  }

  test("Valid EndTime") {
    val etime = 10
    val result = EndTime.from(etime)

    assert(result.fold(_ => false, startTime => startTime.toString == etime.toString))
  }

  test("Invalid EndTime") {
    val etime = -10
    val result = EndTime.from(etime)

    assert(result.fold(error => error == InvalidEndTime(etime), _ => false))
  }

  test("Invalid 0 EndTime") {
    val etime = 0
    val result = EndTime.from(etime)

    assert(result.fold(error => error == InvalidEndTime(etime), _ => false))
  }

}