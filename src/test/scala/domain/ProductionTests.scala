package domain

import domain.DomainError.TaskDoesNotExist
import domain.Production
import domain.SimpleTypes.*
import org.scalatest.funsuite.AnyFunSuite
import xml.XML.traverse

import scala.language.adhocExtensions

class ProductionTests extends AnyFunSuite:
  
  
  test("Create Production - no errors") {
    val xml = scala.xml.XML.loadString({
      <Production>
        <PhysicalResources>
          <Physical id="PRS_1" type="PRST 1"/>
          <Physical id="PRS_2" type="PRST 2"/>
        </PhysicalResources>
        <Tasks>
          <Task id="TSK_1" time="60">
            <PhysicalResource type="PRST 1"/>
            <PhysicalResource type="PRST 2"/>
          </Task>
        </Tasks>
        <HumanResources>
          <Human id="HRS_1" name="Miguel">
            <Handles type="PRST 1"/>
            <Handles type="PRST 2"/>
          </Human>
        </HumanResources>
        <Products>
          <Product id="PRD_1" name="Computador ASUS">
            <Process tskref="TSK_1"/>
          </Product>
        </Products>
        <Orders>
          <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
        </Orders>
      </Production>
    }.toString)

    val result = Production.from(xml)
    assert(result.isRight)
  }

  test("Create Production - Invalid task") {
    val xml = scala.xml.XML.loadString({
      <Production>
        <PhysicalResources>
          <Physical id="PRS_1" type="PRST 1"/>
          <Physical id="PRS_2" type="PRST 2"/>
        </PhysicalResources>
        <Tasks>
          <Task id="TSK_1" time="60">
            <PhysicalResource type="PRST 1"/>
            <PhysicalResource type="PRST 2"/>
          </Task>
        </Tasks>
        <HumanResources>
          <Human id="HRS_1" name="Miguel">
            <Handles type="PRST 1"/>
            <Handles type="PRST 2"/>
          </Human>
        </HumanResources>
        <Products>
          <Product id="PRD_1" name="Computador ASUS">
            <Process tskref="TSK_101112"/>
          </Product>
        </Products>
        <Orders>
          <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
        </Orders>
      </Production>
    }.toString)

    val result = Production.from(xml)
    assert(result.isLeft)
    assert(result.fold(error => error == TaskDoesNotExist("TSK_101112"), _ => false))
  }
