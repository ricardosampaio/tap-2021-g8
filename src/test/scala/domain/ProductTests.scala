package domain

import domain.DomainError.{InexistentTask, InvalidProductId, InvalidProductName}
import domain.SimpleTypes.{HumanId, HumanName, ProductId, ProductName, TaskId, Time}
import org.scalatest.funsuite.AnyFunSuite
import domain.SimpleTypesTests
import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll
import org.scalacheck

import scala.language.adhocExtensions

class ProductTests extends AnyFunSuite {

  test("Create Product - No errors") {
    val xml = scala.xml.XML.loadString("<Product id=\"PRD_1\" name=\"Computador ASUS\"><Process " +
      "tskref=\"TSK_1\"/><Process tskref=\"TSK_2\"/></Product>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    for {
      id <- ProductId.from("PRD_1")
      name <- ProductName.from("Computador ASUS")
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(xml)
    } yield assert(result.id === id && result.name === name && result.tasks.map(t => t.tskref) === List(t1.id, t2.id))

    val result = for {
      id <- ProductId.from("PRD_1")
      name <- ProductName.from("Computador ASUS")
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(xml)
    } yield result

    assert(result.isRight)
  }

  test("Create Product - ProductId errors") {
    val xml = scala.xml.XML.loadString("<Product id=\"PRD1\" name=\"Product 1\"><Process " +
      "tskref=\"TSK_1\"/><Process tskref=\"TSK_2\"/></Product>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    for {
      id <- ProductId.from("PRD1")
      name <- ProductName.from("Computador ASUS")
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(xml)
    } yield assert(result.id != id && result.name === name && result.tasks === List(t1, t2))

    val result = for {
      id <- ProductId.from("PRD1")
      name <- ProductName.from("Computador ASUS")
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(xml)
    } yield result

    assert(result.isLeft)
    assert(result.fold(error => error === InvalidProductId("PRD1"), _ => false))

  }

  test("Create Product - ProductName erros") {
    val xml = scala.xml.XML.loadString("<Product id=\"PRD_1\" name=\"\"><Process " +
      "tskref=\"TSK_1\"/><Process tskref=\"TSK_2\"/></Product>")

    val task1 = for {
      taskId1 <- TaskId.from("TSK_1")
      time1 <- Time.from(60)
    } yield Task(taskId1, time1, List())

    val task2 = for {
      taskId2 <- TaskId.from("TSK_2")
      time2 <- Time.from(360)
    } yield Task(taskId2, time2, List())

    for {
      id <- ProductId.from("PRD_1")
      name <- ProductName.from("")
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(xml)
    } yield assert(result.id === id && result.name != name && result.tasks === List(t1, t2))

    val result = for {
      id <- ProductId.from("PRD_1")
      name <- ProductName.from("")
      t1 <- task1
      t2 <- task2
      result <- Product.from(List(t1,t2))(xml)
    } yield result

    assert(result.isLeft)
    assert(result.fold(error => error === InvalidProductName(""), _ => false))

  }
}