package test.domain

import domain.DomainError.{InvalidHumanId, InvalidHumanName}
import domain.SimpleTypes.{HumanId, HumanName, ResourceType}
import domain.{Human, Physical, PhysicalResourceType, SimpleTypesTests}
import org.scalatest.funsuite.AnyFunSuite

import scala.language.adhocExtensions
import org.scalacheck
import org.scalacheck.Gen

class HumanResourcesTests extends AnyFunSuite {
  
  test("Create HumanResources - no errors") {
    val xml = scala.xml.XML.loadString({
      <Human id="HRS_1" name="Miguel">
        <Handles type="PRST 1"/>
        <Handles type="PRST 2"/>
      </Human>
    }.toString)

    val resourceType1 = ResourceType.from("PRST 1")
    val resourceType2 = ResourceType.from("PRST 2")
    for {
      hrid <- HumanId.from("HRS_1")
      hrname <- HumanName.from("Miguel")
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      result <- Human.from(List(r1,r2))(xml)
    } yield assert(result.id === hrid && result.name === hrname && result.handles.map(h => h.resourceType) === List(r1.resourceType, r2.resourceType))

    val result = for {
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      result <- Human.from(List(r1,r2))(xml)
    } yield result
    assert(result.isRight)
  }

  test("Create HumanResources - HumanId errors") {
    val xml = scala.xml.XML.loadString({
      <Human id="HRS10" name="Miguel">
        <Handles type="PRST 1"/>
        <Handles type="PRST 2"/>
      </Human>
    }.toString)

    for {
      hrid <- HumanId.from("HRS10")
      hrname <- HumanName.from("Miguel")
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      result <- Human.from(List(r1,r2))(xml)
    } yield assert(result.id != hrid && result.name === hrname && result.handles === List(r1, r2))

    val result = for {
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      result <- Human.from(List(r1,r2))(xml)
    } yield result
    assert(result.isLeft)
    assert(result.fold(error => error === InvalidHumanId("HRS10"), _ => false))
  }

  test("Create HumanResource - HumanName errors") {
    val xml = scala.xml.XML.loadString({
      <Human id="HRS_1" name="">
        <Handles type="PRST 1"/>
        <Handles type="PRST 2"/>
      </Human>
    }.toString)

    for {
      hrid <- HumanId.from("HRS_1")
      hrname <- HumanName.from("")
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      result <- Human.from(List(r1,r2))(xml)
    } yield assert(result.id === hrid && result.name != hrname && result.handles === List(r1, r2))

    val result = for {
      r1 <- Physical.from(<Physical id="PRS_1" type="PRST 1"/>)
      r2 <- Physical.from(<Physical id="PRS_2" type="PRST 2"/>)
      result <- Human.from(List(r1,r2))(xml)
    } yield result
    assert(result.isLeft)
  }
}