package domain

import domain.DomainError.*
import domain.SimpleTypes.*
import org.scalatest.funsuite.AnyFunSuite
import org.scalacheck.{Gen, Properties}
import org.scalacheck.Prop.forAll
import org.scalacheck.*
import domain.SimpleTypesTests
import domain.Physical
import domain.SimpleTypes.{ResourceType}

import scala.language.adhocExtensions

class PhysicalResourcesTests extends AnyFunSuite {
  
  test("Create PhysicalResource successfully") {
    val xml = scala.xml.XML.loadString("<Physical id=\"PRS_1005\" type=\"PRST 52\"/>")
    for {
      id <- PhysicalId.from("PRS_1005")
      rType <- ResourceType.from("PRST 52")
      result <- Physical.from(xml)
    } yield assert(result.id === id && result.resourceType === rType)

    val result = Physical.from(xml)
    assert(result.isRight)
  }

  test("Throw invalid resourceType error") {
    val xml = scala.xml.XML.loadString("<Physical id=\"PRS_1005\" type=\"PRST52\"/>")
    for {
      id <- PhysicalId.from("PRS_1005")
      rType <- ResourceType.from("PRST52")
      result <- Physical.from(xml)
    } yield assert(result.id === id && result.resourceType != rType)

    val result = Physical.from(xml)
    assert(result.isLeft)
    assert(result.fold(error => error == DomainError.InvalidResourceType("PRST52"), _ => false))
  }

  test("PhysicalId error") {
    val xml = scala.xml.XML.loadString("<Physical id=\"PRS11\" type=\"PRST 11\"/>")
    for {
      id <- PhysicalId.from("PRS11")
      rType <- ResourceType.from("PRST 11")
      result <- Physical.from(xml)
    } yield assert(result.id != id && result.resourceType === rType)

    val result = Physical.from(xml)
    assert(result.isLeft)
    assert(result.fold(error => error == InvalidPhysicalId("PRS11"), _ => false))
  }
}