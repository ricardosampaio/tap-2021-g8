package domain

import domain.DomainError.*
import domain.*
import domain.SimpleTypes.*
import domain.SimpleTypes.{HumanId, HumanName, OrderId, PhysicalId, ProductId, ProductName, Quantity, ResourceType, TaskId, Time}
import domain.schedule.ScheduleMS01
import org.scalacheck.Gen
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import org.scalacheck.Gen.const
import org.scalacheck.Properties
import org.scalacheck.Prop.*


import scala.language.adhocExtensions

object PropertyBasedTests extends Properties("PropertyBasedTests") :

  val ScheduleServiceToTest = ScheduleMS01

  //Gerar lista a partir da classe
  def genList[T](gen: Gen[T], max: Int, min: Int = 1): Gen[List[T]] =
    for
      n <- Gen.choose[Int](min, max)
      l <- Gen.listOfN(n, gen)
    yield l

  val genTime: Gen[Time]=
    for{
      id <- Gen.chooseNum(30, 3600)
  }yield Time.from(id).getOrElse(throw Error())

  val genProductName: Gen[ProductName] = for {
    i <- Gen.chooseNum(5, 12)
    n <- Gen.listOfN(i, Gen.alphaLowerChar)
    productName <- ProductName.from(n.mkString).fold(_ => Gen.fail, nm => Gen.const(nm))
  } yield productName

  val genHumanName: Gen[HumanName] = for {
    i <- Gen.chooseNum(5, 12)
    n <- Gen.listOfN(i, Gen.alphaLowerChar)
    humanName <- HumanName.from(n.mkString).fold(_ => Gen.fail, nm => Gen.const(nm))
  } yield humanName


  val genOrderId: Gen[OrderId] = for {
    n <- Gen.chooseNum(1,2)
    listn <- Gen.listOfN(n, Gen.chooseNum(1,9))
    id <- OrderId.from("ORD_" + listn.mkString).fold(_ => Gen.fail, oid => Gen.const(oid))
  } yield id

  val genResourceType: Gen[ResourceType] = for {
    n <- Gen.chooseNum(1, 2)
    ln <- Gen.listOfN(n, Gen.chooseNum(1, 9))
    resourceType <- ResourceType.from("PRST " + ln.mkString).fold(_ => Gen.fail, prtid => Gen.const(prtid))
  } yield resourceType

  val genQuantity: Gen[Quantity] = for {
    q <- Gen.chooseNum(1, 3)
    quantity <- Quantity.from(q).fold(_ => Gen.fail, qt => Gen.const(qt))
  } yield quantity

  val genProductId: Gen[ProductId] = for {
    n <- Gen.chooseNum(1, 2)
    listn <- Gen.listOfN(n, Gen.chooseNum(1,9))
    id <- ProductId.from("PRD_" + listn.mkString).fold(_ => Gen.fail, pid => Gen.const(pid))
  } yield id

  val genHumanId: Gen[HumanId] = for {
    n <- Gen.chooseNum(1, 2)
    listn <- Gen.listOfN(n, Gen.chooseNum(1,9))
    id <- HumanId.from("HRS_" + listn.mkString).fold(_ => Gen.fail, hid => Gen.const(hid))
  } yield id

  val genTaskId: Gen[TaskId] = for {
    n <- Gen.chooseNum(1, 2)
    listn <- Gen.listOfN(n, Gen.chooseNum(1,9))
    id <- TaskId.from("TSK_" + listn.mkString).fold(_ => Gen.fail, tid => Gen.const(tid))
  } yield id

  val genPhysicalId: Gen[PhysicalId] = for {
    n <- Gen.chooseNum(1, 2)
    listn <- Gen.listOfN(n, Gen.chooseNum(1,9))
    id <- PhysicalId.from("PRS_" + listn.mkString).fold(_ => Gen.fail, prid => Gen.const(prid))
  } yield id

  def genPhysicalResource(physicalResourceList: List[ResourceType]): Gen[Physical] = for {
    id <- genPhysicalId
    prType <- Gen.oneOf(physicalResourceList)
  } yield Physical(id, prType)

  def genPhysicalResourceType(physicalList: List[Physical]): Gen[PhysicalResourceType] = for {
    prType <- Gen.oneOf(physicalList)
  } yield PhysicalResourceType(prType.resourceType)

  def genOrder(productTasks: List[Product]): Gen[Order] = for{
    id <- genOrderId
    product <- Gen.oneOf(productTasks.map(_.id))
    quantity <- genQuantity
  } yield Order(id, product, quantity)

  def genProduct(process: List[Process]): Gen[Product] = for {
    id <- genProductId
    name <- genProductName
    processList <- Gen.atLeastOne(process)
  } yield Product(id, name, processList.toList)

  def genProcess(taskl: List[Task]): Gen[Process] = for {
    t<-Gen.oneOf(taskl.map(_.id))
  } yield Process(t)

  def genHumanResources(physicalList: List[Physical], prt: List[PhysicalResourceType]): Gen[Human] = for {
    id <- genHumanId
    name <- genHumanName
    pi<- Gen.atLeastOne(prt)
    //prTypeList <- PhysicalResourceType.from(physicalList)
  }yield Human(id, name, pi.toList)

  def genTask(pr: List[Physical], prt: List[PhysicalResourceType]): Gen[Task]= for {
      id <- genTaskId
      time <- genTime
      pi <- Gen.atLeastOne(prt)
      //pk <- PhysicalResourceType.from(pr)
      //vt <- Task.validateTasks(pi,id).getOrElse(throw Error())
    }yield Task(id, time, pi.toList)


  def genProduction: Gen[Production] =
    for{
      rt <- genList(genResourceType, 30, 5)
      pr <- genList(genPhysicalResource(rt), 20, 5)
      prt <- genList(genPhysicalResourceType(pr.distinctBy(_.resourceType)), 50,3)
      t <- genList(genTask(pr.distinctBy(_.id),prt), 10, 5)
      hr <- genList(genHumanResources(pr.distinctBy(_.id),prt), 10, 5)
      proc <- genList(genProcess(t.distinctBy(_.id)),10,5)
      p <- genList(genProduct(proc), 3)
      o <- genList(genOrder(p), 3)
    }yield Production(pr.distinctBy(_.id), t.distinctBy(_.id), hr.distinctBy(_.id), p.distinctBy(_.id), o.distinctBy(_.id))


  property("Property 1 - The same resource cannot be used at the same time by two tasks.") =
    forAll(genProduction)( production => {
      val ltaskschedule=ScheduleServiceToTest.runOrders(production.physicalResources, production.tasks,production.humanResources,production.products,production.orders)
        ltaskschedule.forall(ts1 => {
      //Verifica se os tempos coincidem
      val sttimelist: List[ScheduleTask] =  ltaskschedule.filter(ts2 => ts2 != ts1 && ts1.start.to < ts2.end.to && ts2.start.to < ts1.end.to)
      sttimelist.forall( ts2 =>
        //verifica que os human resources são os mesmos
        val samehrl: List[Human] = ts2.humanResources.toSeq intersect ts1.humanResources.toSeq
        //verifica que os physical resources são os mesmos
        val samepr: List[Physical] = ts2.physicalResources.toSeq intersect ts1.physicalResources.toSeq
          samehrl.size == 0 && samepr.size == 0
      )
    })}
  )

  property("Property 2 - The complete schedule must schedule all the tasks of all the products needed.") =
    forAll(genProduction)( production => {
      val ltaskschedule = ScheduleServiceToTest.runOrders(production.physicalResources, production.tasks, production.humanResources, production.products, production.orders)
      val tasks = for {
        order <- production.orders
      } yield order.quantity.toString.toInt * production.products.filter(p => p.id == order.prdRef).head.tasks.length
      ltaskschedule.length == tasks.sum
    }
   )