import domain.SimpleTypes.*
import domain.{Human, Physical, PhysicalResourceType, Product, Production, SimpleTypes, Task}
import xml.XML

val a =
  <Production>
    <PhysicalResources>
      <Physical id="PRS_1" type="PRST 1"/>
      <Physical id="PRS_2" type="PRST 2"/>
      <Physical id="PRS_3" type="PRST 3"/>
      <Physical id="PRS_4" type="PRST 4"/>
      <Physical id="PRS_5" type="PRST 5"/>
    </PhysicalResources>
    <Tasks>
      <Task id="TSK_1" time="100">
        <PhysicalResource type="PRST 1"/>
        <PhysicalResource type="PRST 4"/>
      </Task>
      <Task id="TSK_2" time="80">
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 5"/>
      </Task>
    </Tasks>
    <HumanResources>
      <Human id="HRS_1" name="Antonio">
        <Handles type="PRST 1"/>
        <Handles type="PRST 2"/>
      </Human>
      <Human id="HRS_2" name="Maria">
        <Handles type="PRST 3"/>
        <Handles type="PRST 4"/>
        <Handles type="PRST 5"/>
      </Human>
    </HumanResources>
    <Products>
      <Product id="PRD_1" name="Product 1">
        <Process tskref="TSK_1"/>
        <Process tskref="TSK_2"/>
      </Product>
      <Product id="PRD_2" name="Product 2">
        <Process tskref="TSK_2"/>
      </Product>
    </Products>
    <Orders>
      <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
      <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
    </Orders>
  </Production>

//val path = getClass.getResource("").getPath
//val testPath = path + "validAgenda_01_in.xml"
//val file = load(testPath)
//val fileR = file.merge
//val fr = fileR.asInstanceOf[Elem]
//new java.io.File(".").getCanonicalPath()
//val path = file.getAbsolutePath();
//val scheduleXml = load("tap-2021-g8/files/assessment/ms01/validAgenda_01_in.xml")
//val scheduleXml = XML.fromAttribute(a,"Order")
val schedule = Production.from(a)
val result = schedule.merge
val r = result.asInstanceOf[Production]
val test = Product.getProduct("PRD_1",r.products)
r.tasks.filter(task => task.id.toString == "TSK_1").head
val tasks = Task.getTasksByProduct(test,r.tasks)

r.humanResources
val pr = r.physicalResources.head
r.humanResources.filter(human => human.handles.map(m=> m.resourceType).contains(pr.resourceType))

val a =
  r.orders.map(order=> {
    val product = Product.getProduct(order.prdRef.toString, r.products)
    val pTasks = Task.getTasksByProduct(product, tasks)
    pTasks.map(task=> {
      val resources = task.resources
      val humanResources = resources.map(resource => Human.getHumanByResource(resource, r.humanResources))
      <TaskSchedule order={order.id.toString} productNumber={ProductName.getNumber(product.name.toString)} task={task.id.toString}>
        <PhysicalResources>
          {resources.map((physical)=>Physical.physicalXml(Physical.getPhysicalByResource(physical, r.physicalResources)))}
        </PhysicalResources>
        <HumanResources>
          {humanResources.map((human)=> Human.humanXML(human))}
        </HumanResources>
      </TaskSchedule>

    })
  })

print(
  <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
    {a}
  </Schedule>)
