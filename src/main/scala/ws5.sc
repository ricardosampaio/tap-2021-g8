import domain.*
import domain.SimpleTypes.*
import domain.DomainError.*
import xml.XML.*
import scala.xml.*

def runTasks(productTasks: List[Task], pName: String ,order: Order , start: Time, end: Time,physicalResources: List[Physical],
             tasks: List[Task],
             humanResources: List[Human],
             products: List[Product],
             orders: List[Order]): List[ScheduleTask] =
  for{
    t <- productTasks
  } yield ScheduleTask(order.id, pName, t.id, start, end + t.time, Physical.getPhysicals(t.resources,physicalResources), Human.getHumans(t.resources,humanResources))


def runOrders(physicalResources: List[Physical],
              tasks: List[Task],
              humanResources: List[Human],
              products: List[domain.Product],
              orders: List[Order]): List[ScheduleTask] =
  for{
    order <- orders
    p <- for {
      n <- (1 to order.quantity.toString.toInt).map(x => x).toList
      result <-
        val product = Product.getProduct(order.prdRef, products)
        val productTasks = Task.getTasksByProduct(product, tasks)
        runTasks(productTasks, n.toString, order, Time.zero, Time.zero, physicalResources, tasks, humanResources, products, orders)
    } yield result
  } yield p

def findDiffTasks(schedules: List[ScheduleTask], s: ScheduleTask): List[ScheduleTask] = {
  val p = s.physicalResources.map(x => x.id)
  val h = s.humanResources.map(x => x.id)
  val result = schedules.filter(x => !x.physicalResources.map(y => y.id).exists(p.contains) && !x.humanResources.map(y => y.id).exists(h.contains) )
  if(result.isEmpty) List(s) else s :: result
}

def toScheduleTask2(schedules: List[List[ScheduleTask]]): List[Node] =
  for {
    s <- schedules
    r <- s
    result <-
      val startTime = schedules.takeWhile(x => x != s).map(x => x.map(y => y.end.to).max).sum
      val endTime = startTime + r.end.to
      <TaskSchedule order={r.order.toString} productNumber={r.productNumber.toString} task={r.task.toString} start={startTime.toString} end={endTime.toString}>
        <PhysicalResources>
          {r.physicalResources.map((physical)=>Physical.physicalXml(physical))}
        </PhysicalResources>
        <HumanResources>
          {r.humanResources.map((human)=> Human.humanXML(human))}
        </HumanResources>
      </TaskSchedule>
  } yield result

def filterTest(t1: List[ScheduleTask],t2: List[ScheduleTask]) : List[ScheduleTask] = {
  t2.filter(x => !t1.contains(x))
}

def toXML2(schedules: List[ScheduleTask]): Result[Elem] = {
  val r = <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
    {
      val s = schedules.map(x => findDiffTasks(schedules, x)).distinct
      val r = s.map(x => filterTest(s.takeWhile(y => y != x).flatten, x))
      toScheduleTask2(r.sortWith(_.length > _.length))
    }
  </Schedule>
  if(schedules.isEmpty) Left(XMLError("")) else Right (r)
}

def create(xml: Elem): Result[Elem] =
  for {
    physicalResources <- traverse( (xml \\ "Physical"), Physical.from )
    humanResources <- traverse( (xml \\ "Human"), Human.from(physicalResources) )
    tasks <- traverse( (xml \\ "Task"), Task.from2(physicalResources, humanResources) )
    products <- traverse( (xml \\ "Product"), domain.Product.from(tasks) )
    orders <- traverse( (xml \\ "Order"), Order.from(products) )
    t <- Human.validateHumans2(tasks,humanResources)
    result <- toXML2(runOrders(physicalResources, tasks, humanResources, products,orders))
  } yield result


val a1 = <Production xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../production.xsd ">
  <PhysicalResources>
    <Physical id="PRS_1" type="PRST 1"/>
    <Physical id="PRS_2" type="PRST 2"/>
    <Physical id="PRS_3" type="PRST 3"/>
  </PhysicalResources>
  <Tasks>
    <Task id="TSK_1" time="5">
      <PhysicalResource type="PRST 1"/>
    </Task>
    <Task id="TSK_2" time="3">
      <PhysicalResource type="PRST 2"/>
    </Task>
    <Task id="TSK_3" time="2">
      <PhysicalResource type="PRST 3"/>
    </Task>
  </Tasks>
  <HumanResources>
    <Human id="HRS_1" name="Antonio">
      <Handles type="PRST 3"/>
    </Human>
    <Human id="HRS_2" name="Maria">
      <Handles type="PRST 1"/>
      <Handles type="PRST 2"/>
    </Human>
  </HumanResources>
  <Products>
    <Product id="PRD_1" name="Product 1">
      <Process tskref="TSK_1"/>
    </Product>
    <Product id="PRD_2" name="Product 2">
      <Process tskref="TSK_2"/>
    </Product>
    <Product id="PRD_3" name="Product 3">
      <Process tskref="TSK_3"/>
    </Product>
  </Products>
  <Orders>
    <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
    <Order id="ORD_2" prdref="PRD_2" quantity="1"/>
    <Order id="ORD_3" prdref="PRD_3" quantity="1"/>
  </Orders>
</Production>

print(create(a1))