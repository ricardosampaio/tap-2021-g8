package domain

import domain.SimpleTypes.{TaskId, Time, ResourceType}
import domain.*
import domain.DomainError.*
import scala.xml.Node
import xml.XML.*

final case class Task(id: TaskId, time: Time, resources: List[PhysicalResourceType])

object Task:
  def from(physicals: List[Physical], humans: List[Human])(n: Node): Result[Task] =
    for {
      taskId <- TaskId.from(n \@ "id")
      time <- Time.from((n \@ "time").toInt)
      resources <- traverse( (n \\ "PhysicalResource"), PhysicalResourceType.from(physicals) )
      r <- validateTasksHumans(resources, humans, taskId)
      r2 <- validateTasksPhysicals(r, physicals, taskId)
    } yield Task(taskId, time, r2)

  def from2(physicals: List[Physical], humans: List[Human])(n: Node): Result[Task] =
    for {
      taskId <- TaskId.from(n \@ "id")
      time <- Time.from((n \@ "time").toInt)
      resources <- traverse( (n \\ "PhysicalResource"), PhysicalResourceType.from(physicals) )
      r <- validateTasks2(resources, humans)
      r2 <- validateTasks3(r, physicals)
    } yield Task(taskId, time, r2)

  def getTasksByProduct(p: Product, tasks: List[Task]): List[Task] =
    tasks.filter(task => p.tasks.map(pt => pt.tskref).contains(task.id))

  def findTaskId(s: TaskId, tasks: List[Task]): Result[TaskId] =
    val x = tasks.find(p => p.id == s)
    x match
      case Some(f) => Right(f.id)
      case None => Left(TaskDoesNotExist(s.toString))

  def findTask(task: TaskId, tasks: List[Task]): Task =
    val x = tasks.find(t => t.id == task )
    x match
      case Some(f) => f
      case None => tasks.head

  def validateTasksHumans(resources: List[PhysicalResourceType], humans: List[Human], id: TaskId) : Result[List[PhysicalResourceType]] =
    val result = for{
      r <- resources
    } yield resources.filter(y => y.resourceType == r.resourceType).length <= humans.filter(y => y.handles.map(m=> m.resourceType).contains(r.resourceType)).length
    if(!result.contains(false)) Right(resources) else Left(ResourceUnavailable(id.toString,resources.head.resourceType.toString))

  def validateTasksPhysicals(resources: List[PhysicalResourceType], physicals: List[Physical], id: TaskId) : Result[List[PhysicalResourceType]] =
    val result = for{
      r <- resources
    } yield resources.filter(y => y.resourceType == r.resourceType).length <= physicals.filter(y => y.resourceType == r.resourceType).length
    if(!result.contains(false)) Right(resources) else Left(ResourceUnavailable(id.toString,resources.head.resourceType.toString))


  def validateTasks2(resources: List[PhysicalResourceType], humans: List[Human]) : Result[List[PhysicalResourceType]] =
    val result = for{
      r <- resources
    } yield resources.filter(y => y.resourceType == r.resourceType).length <= humans.filter(y => y.handles.map(m=> m.resourceType).contains(r.resourceType)).length
    if(!result.contains(false)) Right(resources) else Left(ImpossibleSchedule)

  def validateTasks3(resources: List[PhysicalResourceType], physicals: List[Physical]) : Result[List[PhysicalResourceType]] =
    val result = for{
      r <- resources
    } yield resources.filter(y => y.resourceType == r.resourceType).length <= physicals.filter(y => y.resourceType == r.resourceType).length
    if(!result.contains(false)) Right(resources) else Left(ImpossibleSchedule)