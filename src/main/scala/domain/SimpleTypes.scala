package domain

import domain.DomainError.*
import domain.Result

import scala.annotation.targetName
import scala.util.Try
object SimpleTypes:
  
  opaque type ProductId = String
  object ProductId:
    def from(s: String): Result[ProductId] =
      val rx = "PRD_[0-9]+".r
      if(rx.matches(s)) then Right(s) else Left(InvalidProductId(s))

  opaque type ProductName = String
  object ProductName:
    def from(value: String): Result[ProductName] =
      if(!value.isEmpty &&  value != null) Right(value) else Left(InvalidProductName(value))
    def getNumber(value: String) : String =
      value.split(" ").last

  opaque type Quantity = Int
  object Quantity:
    def from(i:Int): Result[Quantity] =
      if(i>0) Right(i) else Left(InvalidQuantity(i))
    def from(value: String): Result[Quantity] =
      Try(value.toInt).fold(_ => Left(FailedQuantityConversion(value)), i => {
        if(i > 0) Right(i) else Left(InvalidQuantity(i))})

  opaque type TaskId = String
  object TaskId:
    def from(s: String): Result[TaskId] =
      val rx = "TSK_[0-9][0-9]*".r
      if(rx.matches(s)) then Right(s) else Left(InvalidTaskId(s))

  opaque type PhysicalId = String
  object PhysicalId:
    def from(s: String): Result[PhysicalId] =
      val rx = "PRS_[0-9][0-9]*".r
      if(rx.matches(s)) then Right(s) else Left(InvalidPhysicalId(s))
  
  opaque type ResourceType = String
  object ResourceType:
    def from(s:String): Result[ResourceType] =
      val rx = "PRST[_ ][1-9][0-9]*".r
      if(rx.matches(s)) then Right (s) else Left (InvalidResourceType(s))

  opaque type HumanId = String
  object HumanId:
    def from(s: String): Result[HumanId] =
      val rx = "HRS_[0-9][0-9]*".r
      if(rx.matches(s)) then Right(s) else Left(InvalidHumanId(s))

  opaque type HumanName = String
  object HumanName:
    def from(value: String): Result[HumanName] =
      if(!value.isEmpty && value != null) Right(value) else Left(InvalidHumanName(value))

  opaque type OrderId = String
  object OrderId:
    def from(s: String): Result[OrderId] =
      val rx = "ORD_[0-9][0-9]*".r
      if(rx.matches(s)) then Right(s) else Left(InvalidOrderId(s))

  opaque type NonEmptyString = String
  object NonEmptyString:
    def from(s:String): Result[NonEmptyString] =
      if(!s.isEmpty) then Right(s) else Left(EmptyString(s))

  opaque type Time = Int
  object Time:
    def from(t:Int): Result[Time] =
      if(t>0) Right(t) else Left(DomainError.NegativeTime(t))
    def zero: Time = 0
  extension (t1: Time)
    @targetName("Time")
    def +(other:Time): Time = t1+other
    @targetName("timeToInt")
    def to: Int = t1
    @targetName("isBefore")
    infix def < (t2: Time): Boolean = (t1 < t2)
    @targetName("isAfter")
    infix def > (t2: Time): Boolean = (t1 > t2)

  opaque type ProductNumber = Int
  object ProductNumber:
    def from(value: Int): Result[ProductNumber] =
      if(value > 0) Right(value) else Left(InvalidProductNumber(value))
  
  extension (productNumber: ProductNumber)
    @targetName("productNumberTo")
    def to: Int = productNumber
  
  opaque type StartTime = Int
  object StartTime:
    def from(value: Int): Result[StartTime] =
      if(value >= 0) Right(value) else Left(InvalidStartTime(value))
  
  extension (startTime: StartTime)
    @targetName("startTimeTo")
    def to: Int = startTime
  
  opaque type EndTime = Int
  object EndTime:
    def from(value: Int): Result[EndTime] =
      if(value > 0) Right(value) else Left(InvalidEndTime(value))
  
  extension (endTime: EndTime)
    @targetName("endTimeTo")
    def to: Int = endTime

  //opaque type PositiveInteger = Int
  //object PositiveInteger:
  //  def from(i: Int): Result[PositiveInteger] =
  //  if(i > 0) then Right(i) else Left(InvalidPositiveInteger(i))

  //enum ResourceType(val value: String):
  //  case type1 extends ResourceType("Type1")
  //  case type2 extends ResourceType("Type2")
  //  case type3 extends ResourceType("Type3")