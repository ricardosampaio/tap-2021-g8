package domain

import domain.SimpleTypes.{OrderId, ProductId, Quantity}
import domain.*
import scala.xml.Node
import scala.xml

final case class Order(id: OrderId, prdRef: ProductId, quantity: Quantity)

object Order:
  def from(products: List[domain.Product])(n:Node): Result[Order] =
    for {
      id <- OrderId.from(n \@ "id")
      prdRef <- ProductId.from(n \@ "prdref")
      p <- domain.Product.findProduct(prdRef, products)
      quantity <- Quantity.from(n \@ "quantity")
    }yield Order(id, p, quantity)
