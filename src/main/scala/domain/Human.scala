package domain

import domain.SimpleTypes.{HumanId, HumanName, NonEmptyString}

import scala.xml.Node
import domain.*
import domain.DomainError.*

import scala.xml
import _root_.xml.XML.traverse

final case class Human(id: HumanId, name: HumanName, handles: List[PhysicalResourceType])

object Human:
  def from(physicals: List[Physical])(n: Node): Result[Human] =
    for {
      humanid <- HumanId.from(n \@ "id")
      name <- HumanName.from(n \@ "name")
      handles <- traverse( (n \\ "Handles"), PhysicalResourceType.from(physicals) )
    } yield Human(humanid, name, handles)

  def humanXML(human: Human): xml.Elem =
      <Human name={human.name.toString} />

  def getHumanByResource(physicalResourceType: PhysicalResourceType, humans: List[Human]): Human =
    val x = humans.find((human)=> human.handles.map(m=> m.resourceType).contains(physicalResourceType.resourceType))
    x match
      case Some(f) => f
      case None => humans.head

  def getHumanByResource2(physicalResourceType: PhysicalResourceType, humans: List[Human]): List[Human] =
    val x = humans.filter((human)=> human.handles.map(m=> m.resourceType).contains(physicalResourceType.resourceType))
    if(x.isEmpty) humans else x

  def validateHumans(tasks: List[Task], humans: List[Human]) : Result[List[Task]] =
    val x = tasks.filter(t => t.resources.length > humans.length)
    if(x.isEmpty) Right(tasks) else Left(ResourceUnavailable(x.head.id.toString,x.head.resources.last.resourceType.toString))

  def validateHumans2(tasks: List[Task], humans: List[Human]) : Result[List[Task]] =
    val x = tasks.filter(t => t.resources.length > humans.length)
    if(x.isEmpty) Right(tasks) else Left(ImpossibleSchedule)

  def getHumans2(resources: List[PhysicalResourceType], humans: List[Human]) : List[Human] =
    val result = for {
      r <- resources
    } yield getHumanByResource2(r, humans)
    recursiveTest(result).flatten

  def recursiveTest(humans: List[List[Human]]) : List[List[Human]] = {
    humans match
      case Nil => Nil
      case x::Nil => humans.map(f => if(f.length > 1) f.take(1) else f )
      case x::t => List(x.head) :: recursiveTest(t.map(z => z.filter(i => i != x.head)))
  }