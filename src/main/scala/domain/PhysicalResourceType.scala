package domain

import domain.SimpleTypes.{ResourceType}
import domain.Result
import scala.xml.Node

final case class PhysicalResourceType(resourceType: ResourceType)

object PhysicalResourceType:
  def from(physicals: List[Physical])(n : Node): Result[PhysicalResourceType] =
    for
      resourceType <- ResourceType.from(n \@ "type")
      r <- Physical.findResourceType(resourceType,physicals)
    yield PhysicalResourceType(r)