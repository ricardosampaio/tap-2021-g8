package domain

import domain.SimpleTypes.*
import domain.DomainError.*
import scala.xml.*

final case class ScheduleTask(order: OrderId, productNumber: String, task: TaskId, start: Time, end: Time, physicalResources: List[Physical], humanResources: List[Human])

object ScheduleTask:
  def from(order: OrderId, productNumber: String, task: TaskId, start: Time, end: Time, physicalResources: List[Physical], humanResources: List[Human]) : ScheduleTask =
    ScheduleTask(order,productNumber,task,start,end,physicalResources,humanResources)

  def toScheduleTask(schedules: List[ScheduleTask]): List[Node] =
    for {
      s <- schedules
      result <-
        val startTime = schedules.takeWhile(x => x != s).map(x => x.end.to).sum
        val endTime = startTime + s.end.to
        <TaskSchedule order={s.order.toString} productNumber={s.productNumber.toString} task={s.task.toString} start={startTime.toString} end={endTime.toString}>
          <PhysicalResources>
            {s.physicalResources.map((physical)=>Physical.physicalXml(physical))}
          </PhysicalResources>
          <HumanResources>
            {s.humanResources.map((human)=> Human.humanXML(human))}
          </HumanResources>
        </TaskSchedule>
    } yield result

  def toScheduleTask2(schedules: List[List[ScheduleTask]]): List[Node] =
    for {
      s <- schedules
      r <- s
      result <-
        val startTime = schedules.takeWhile(x => x != s).map(x => x.map(y => y.end.to).max).sum
        val endTime = startTime + r.end.to
        <TaskSchedule order={r.order.toString} productNumber={r.productNumber.toString} task={r.task.toString} start={startTime.toString} end={endTime.toString}>
          <PhysicalResources>
            {r.physicalResources.map((physical)=>Physical.physicalXml(physical))}
          </PhysicalResources>
          <HumanResources>
            {r.humanResources.map((human)=> Human.humanXML(human))}
          </HumanResources>
        </TaskSchedule>
    } yield result

  def toXML(schedules: List[ScheduleTask]): Result[Elem] =
    val r = <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
      {toScheduleTask(schedules)}
    </Schedule>
    if(schedules.isEmpty) Left(XMLError("")) else Right (r)

  def compareScheduleTasksLists(t1: List[ScheduleTask],t2: List[ScheduleTask]) : List[ScheduleTask] =
    t2.filter(x => !t1.contains(x))

  def toXML2(schedules: List[ScheduleTask]): Result[Elem] =
    val r = <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
      {
      val s = schedules.map(x => findDiffTasks(schedules, x)).distinct
      val r = s.map(x => compareScheduleTasksLists(s.takeWhile(y => y != x).flatten, x))
      toScheduleTask2(r.sortWith(_.length > _.length))
      }
    </Schedule>
    if(schedules.isEmpty) Left(XMLError("")) else Right (r)

  def findDiffTasks(schedules: List[ScheduleTask], s: ScheduleTask): List[ScheduleTask] = {
    val p = s.physicalResources.map(x => x.id)
    val h = s.humanResources.map(x => x.id)
    val result = schedules.filter(x => !x.physicalResources.map(y => y.id).exists(p.contains) && !x.humanResources.map(y => y.id).exists(h.contains) )
    if(result.isEmpty) List(s) else s :: result
  }