package domain

import domain.*
import scala.xml.Elem
import xml.XML.*

final case class Production(physicalResources: List[Physical],
                                    tasks: List[Task],
                                    humanResources: List[Human],
                                    products: List[Product],
                                    orders: List[Order])

object Production:
  def from(n: Elem): Result[Production] =
    for {
      physicalResources <- traverse( (n \\ "Physical"), Physical.from )
      humanResources <- traverse( (n \\ "Human"), Human.from(physicalResources) )
      tasks <- traverse( (n \\ "Task"), Task.from(physicalResources, humanResources) )
      products <- traverse( (n \\ "Product"), Product.from(tasks) )
      orders <- traverse( (n \\ "Order"), Order.from(products) )
    } yield Production(physicalResources,tasks,humanResources,products,orders)

  def from2(n: Elem): Result[Production] =
    for {
      physicalResources <- traverse( (n \\ "Physical"), Physical.from )
      humanResources <- traverse( (n \\ "Human"), Human.from(physicalResources) )
      tasks <- traverse( (n \\ "Task"), Task.from2(physicalResources, humanResources) )
      products <- traverse( (n \\ "Product"), Product.from(tasks) )
      orders <- traverse( (n \\ "Order"), Order.from(products) )
      t <- Human.validateHumans2(tasks,humanResources)
    } yield Production(physicalResources,tasks,humanResources,products,orders)