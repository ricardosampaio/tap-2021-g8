package domain

import domain.SimpleTypes.{NonEmptyString, ProductId, ProductName, TaskId}
import domain.*
import domain.DomainError.{ProductDoesNotExist}
import scala.xml.Node
import xml.XML.*

final case class Process (tskref: TaskId)

object Process:
  def from(tasks: List[Task])(n: Node): Result[Process] =
    for {
      tskRef <- TaskId.from(n \@ "tskref")
      t <- Task.findTaskId(tskRef,tasks)
    } yield Process(t)

final case class Product (id: ProductId, name: ProductName, tasks: List[Process])

object Product:
  def from(tasks: List[Task])(n: Node): Result[Product] =
    for {
      prdId <- ProductId.from(n \@ "id")
      name <- ProductName.from(n \@ "name")
      tasks <- traverse( (n \\ "Process"), Process.from(tasks) )
    } yield Product(prdId, name, tasks)

  def findProduct(s: ProductId, products: List[Product]): Result[ProductId] =
    val x = products.find(p => p.id == s)
    x match
      case Some(f) => Right(f.id)
      case None => Left(ProductDoesNotExist(s.toString))

  def getProduct(s: ProductId, products: List[Product]): Product = {
    val x = products.find(p => p.id == s)
    x match
      case Some(f) => f
      case None => products.head
  }