package domain

type Result[A] = Either[DomainError,A]

enum DomainError:
  case IOFileProblem(error: String)
  case XMLError(error: String)
  case InvalidProductId(s: String)
  case InvalidProductName(error: String)
  case InvalidTaskId(s: String)
  case InvalidTime(error: Int)
  case InvalidPhysicalId(s: String)
  case InvalidHumanId(s: String)
  case InvalidHumanName(error: String)
  case InvalidOrderId(s: String)
  case InvalidPositiveInteger(i: Int)
  case InvalidQuantity(i: Int)
  case InvalidResourceType(s: String)
  case FailedQuantityConversion(error: String)
  case EmptyString(s: String)
  case NegativeTime(t: Int)
  case InexistentTask(error: String)
  case InexistentProduct(error: String)
  case HumanNotAvailable(error: String)
  case InvalidProductNumber(error: Int)
  case InvalidStartTime(error: Int)
  case InvalidEndTime(error: Int)
  case FailedTimeConversion(error:String)
  case ProductDoesNotExist(error:String)
  case TaskDoesNotExist(error:String)
  case TaskUsesNonExistentPRT(error:String)
  case ResourceUnavailable(task:String, resource:String)
  case ImpossibleSchedule