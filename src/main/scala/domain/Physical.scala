package domain

import domain.SimpleTypes.{PhysicalId, ResourceType}

import scala.xml.Node
import domain.Result
import domain.*
import domain.DomainError.*

import scala.xml

final case class Physical(id: PhysicalId, resourceType: ResourceType)

object Physical:
  def create(id:PhysicalId, resourceType:ResourceType): Result[Physical] =
    new Right(Physical(id, resourceType))
    
  def from(n:Node): Result[Physical] =
    for {
      id <- PhysicalId.from(n \@ "id")
      resourceType <- ResourceType.from(n \@ "type")
    }yield Physical(id, resourceType)

  def physicalXml(physical: Physical): xml.Elem =
    <Physical id={physical.id.toString}/>

  def findResourceType(r: ResourceType, resources: List[Physical]) : Result[ResourceType] =
    val x = resources.find(p => p.resourceType == r)
    x match
      case Some(f) => Right(f.resourceType)
      case None => Left(TaskUsesNonExistentPRT(r.toString))

  def getPhysicalByResource(physicalResourceType: PhysicalResourceType, physicals: List[Physical]): List[Physical] =
    physicals.filter(phy => phy.resourceType.equals(physicalResourceType.resourceType))

  def getPhysicals(resources: List[PhysicalResourceType], physicals: List[Physical]) : List[Physical] =
    val result = for {
      r <- resources
    } yield getPhysicalByResource(r, physicals)
    recursiveTest(result).flatten

  def recursiveTest(physicals: List[List[Physical]]) : List[List[Physical]] = {
    physicals match
      case Nil => Nil
      case x::Nil => physicals.map(f => if(f.length > 1) f.take(1) else f )
      case x::t => List(x.head) :: recursiveTest(t.map(z => z.filter(i => i != x.head)))
  }