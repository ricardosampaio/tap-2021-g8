package domain.schedule

import scala.xml.*
import domain.*
import domain.SimpleTypes.*
import xml.XML.traverse


object ScheduleMS03 extends Schedule:

  // TODO: Create the code to implement a functional domain model for schedule creation
  //       Use the xml.XML code to handle the xml element
  //       Refer to https://github.com/scala/scala-xml/wiki/XML-Processing for xml creation
  def create(xml: Elem): Result[Elem] =
    for {
      physicalResources <- traverse( (xml \\ "Physical"), Physical.from )
      humanResources <- traverse( (xml \\ "Human"), Human.from(physicalResources) )
      tasks <- traverse( (xml \\ "Task"), Task.from2(physicalResources, humanResources) )
      products <- traverse( (xml \\ "Product"), domain.Product.from(tasks) )
      orders <- traverse( (xml \\ "Order"), Order.from(products) )
      t <- Human.validateHumans2(tasks,humanResources)
      result <- ScheduleTask.toXML2(runOrders(physicalResources, tasks, humanResources, products,orders))
    } yield result

  def runTasks(productTasks: List[Task], pName: String ,order: Order , start: Time, end: Time,physicalResources: List[Physical],
               tasks: List[Task],
               humanResources: List[Human],
               products: List[Product],
               orders: List[Order]): List[ScheduleTask] =
    for{
      t <- productTasks
    } yield ScheduleTask(order.id, pName, t.id, start, end + t.time, Physical.getPhysicals(t.resources,physicalResources), Human.getHumans2(t.resources,humanResources))

  def runOrders(physicalResources: List[Physical],
                tasks: List[Task],
                humanResources: List[Human],
                products: List[domain.Product],
                orders: List[Order]): List[ScheduleTask] =
    for{
      order <- orders
      p <- for {
        n <- (1 to order.quantity.toString.toInt).map(x => x).toList
        result <-
          val product = Product.getProduct(order.prdRef, products)
          val productTasks = Task.getTasksByProduct(product, tasks)
          runTasks(productTasks, n.toString, order, Time.zero, Time.zero, physicalResources, tasks, humanResources, products, orders)
      } yield result
    } yield p
