import domain.*
import domain.SimpleTypes.*
import scala.xml.Elem
import xml.XML.*

def newStartTime(start: Time, duration: Time): Time = {
  start + duration
}

def newEndTime(end: Time, duration: Time): Time = {
  end + duration
}

def taskScheduleXML(order: OrderId, product: domain.Product, task: Task, start: Time, end: Time, resources: List[PhysicalResourceType], physicalResources: List[Physical], humanResources: List[Human]): Elem = {
  <TaskSchedule order={order.toString} productNumber={ProductName.getNumber(product.name.toString)} task={task.id.toString}>
    <PhysicalResources>
      {resources.map((physical)=>Physical.physicalXml(Physical.getPhysicalByResource(physical, physicalResources)))}
    </PhysicalResources>
    <HumanResources>
      {humanResources.map((human)=> Human.humanXML(human))}
    </HumanResources>
  </TaskSchedule>
}

def createScheduledTask(orderId: OrderId, product: domain.Product, task: Task, humans: List[Human], physicalResources: List[Physical],time: Time): Elem = {
  val resources = task.resources
  val humanResources = resources.map(resource => Human.getHumanByResource(resource, humans))
  val endTime = newEndTime(time, task.time)
  taskScheduleXML(orderId, product, task, time, endTime, resources, physicalResources, humanResources)
}

def createSchedule(orders: List[Order], products: List[domain.Product], tasks: List[Task], humans: List[Human], physicalResources: List[Physical]) : List[Elem] = {
  for {
    order <- orders
    x <- {
      val product = domain.Product.getProduct(order.prdRef, products)
      val pTasks = Task.getTasksByProduct(product, tasks)
      pTasks.map(task=> {
        val startTimeD = newStartTime(Time.zero, task.time)
        createScheduledTask(order.id, product, task, humans, physicalResources, startTimeD)
      })
    }
  } yield x
}

def output(tasksScheduled: List[Elem]): Elem = {
  <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
    {tasksScheduled}
  </Schedule>
}

def create(n: Elem): Result[Elem] = {
  for {
    physicalResources <- traverse( (n \\ "Physical"), Physical.from )
    tasks <- traverse( (n \\ "Task"), Task.from )
    humanResources <- traverse( (n \\ "Human"), Human.from )
    products <- traverse( (n \\ "Product"), domain.Product.from )
    orders <- traverse( (n \\ "Order"), Order.from )
  } yield output(createSchedule(orders, products, tasks, humanResources, physicalResources))
}

val a =
  <Production>
    <PhysicalResources>
      <Physical id="PRS_1" type="PRST 1"/>
      <Physical id="PRS_2" type="PRST 2"/>
      <Physical id="PRS_3" type="PRST 3"/>
      <Physical id="PRS_4" type="PRST 4"/>
      <Physical id="PRS_5" type="PRST 5"/>
    </PhysicalResources>
    <Tasks>
      <Task id="TSK_1" time="100">
        <PhysicalResource type="PRST 1"/>
        <PhysicalResource type="PRST 4"/>
      </Task>
      <Task id="TSK_2" time="80">
        <PhysicalResource type="PRST 2"/>
        <PhysicalResource type="PRST 5"/>
      </Task>
    </Tasks>
    <HumanResources>
      <Human id="HRS_1" name="Antonio">
        <Handles type="PRST 1"/>
        <Handles type="PRST 2"/>
      </Human>
      <Human id="HRS_2" name="Maria">
        <Handles type="PRST 3"/>
        <Handles type="PRST 4"/>
        <Handles type="PRST 5"/>
      </Human>
    </HumanResources>
    <Products>
      <Product id="PRD_1" name="Product 1">
        <Process tskref="TSK_1"/>
        <Process tskref="TSK_2"/>
      </Product>
      <Product id="PRD_2" name="Product 2">
        <Process tskref="TSK_2"/>
      </Product>
    </Products>
    <Orders>
      <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
      <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
    </Orders>
  </Production>

create(a)