import domain.*
import domain.SimpleTypes.*
import domain.DomainError.*
import xml.XML.*
import scala.xml.*

def runTasks(productTasks: List[Task], pName: String ,order: Order , start: Time, end: Time,physicalResources: List[Physical],
             tasks: List[Task],
             humanResources: List[Human],
             products: List[Product],
             orders: List[Order]): List[ScheduleTask] =
  for{
    t <- productTasks
  } yield ScheduleTask(order.id, pName, t.id, start, end + t.time, Physical.getPhysicals(t.resources,physicalResources), Human.getHumans2(t.resources,humanResources))

def runOrders(physicalResources: List[Physical],
              tasks: List[Task],
              humanResources: List[Human],
              products: List[domain.Product],
              orders: List[Order]): List[ScheduleTask] =
  for{
    order <- orders
    p <- for {
      n <- (1 to order.quantity.toString.toInt).map(x => x).toList
      result <-
        val product = Product.getProduct(order.prdRef, products)
        val productTasks = Task.getTasksByProduct(product, tasks)
        runTasks(productTasks, n.toString, order, Time.zero, Time.zero, physicalResources, tasks, humanResources, products, orders)
    } yield result
  } yield p


def findSimilarTasks(schedules: List[ScheduleTask], s: ScheduleTask): List[ScheduleTask] = {
  val p = s.physicalResources.map(x => x.id)
  val h = s.humanResources.map(x => x.id)
  val result = schedules.filter(x => x.physicalResources.map(y => y.id).exists(p.contains) && x.humanResources.map(y => y.id).exists(h.contains) )
  if(result.isEmpty) List(s) else s :: result
}

def findAnotherHuman(humans: List[Human], h : Human, r : PhysicalResourceType) : Human = {
  var result = humans.filter(p => p.handles.contains(r) && p != h)
  if (result.isEmpty) h else result.head
}

def updateResources(schedules: List[ScheduleTask]): List[ScheduleTask] = {
  var h = schedules.map(s => s.humanResources)
  var p = schedules.map(s => s.humanResources)
  if (schedules.length == 1) schedules else  schedules
}

def create(xml: Elem): Result[Elem] =
  for {
    physicalResources <- traverse( (xml \\ "Physical"), Physical.from )
    humanResources <- traverse( (xml \\ "Human"), Human.from(physicalResources) )
    tasks <- traverse( (xml \\ "Task"), Task.from2(physicalResources, humanResources) )
    products <- traverse( (xml \\ "Product"), domain.Product.from(tasks) )
    orders <- traverse( (xml \\ "Order"), Order.from(products) )
    t <- Human.validateHumans2(tasks,humanResources)
    result <-
      val r = runOrders(physicalResources, tasks, humanResources, products,orders)
      val x = for{
        s <- r
      } yield findSimilarTasks(r,s)
      val sd = for {
        s <- x
        ur <- updateResources(s)
      } ur
      ScheduleTask.toXML2(r)
  } yield result

val a1 = <Production xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../production.xsd ">
  <PhysicalResources>
    <Physical id="PRS_1" type="PRST 1"/>
    <Physical id="PRS_2" type="PRST 2"/>
    <Physical id="PRS_3" type="PRST 3"/>
  </PhysicalResources>
  <Tasks>
    <Task id="TSK_1" time="5">
      <PhysicalResource type="PRST 1"/>
    </Task>
    <Task id="TSK_2" time="3">
      <PhysicalResource type="PRST 2"/>
    </Task>
    <Task id="TSK_3" time="2">
      <PhysicalResource type="PRST 3"/>
    </Task>
  </Tasks>
  <HumanResources>
    <Human id="HRS_1" name="Antonio">
      <Handles type="PRST 3"/>
    </Human>
    <Human id="HRS_2" name="Maria">
      <Handles type="PRST 1"/>
      <Handles type="PRST 2"/>
    </Human>
  </HumanResources>
  <Products>
    <Product id="PRD_1" name="Product 1">
      <Process tskref="TSK_1"/>
    </Product>
    <Product id="PRD_2" name="Product 2">
      <Process tskref="TSK_2"/>
    </Product>
    <Product id="PRD_3" name="Product 3">
      <Process tskref="TSK_3"/>
    </Product>
  </Products>
  <Orders>
    <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
    <Order id="ORD_2" prdref="PRD_2" quantity="1"/>
    <Order id="ORD_3" prdref="PRD_3" quantity="1"/>
  </Orders>
</Production>

print(create(a1))