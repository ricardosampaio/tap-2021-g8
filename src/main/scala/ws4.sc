import domain.*
import domain.SimpleTypes.*
import domain.DomainError.*
import scala.xml.*
import _root_.xml.XML.*

final case class ScheduleTask(order: OrderId, productNumber: String, task: TaskId, start: Time, end: Time, physicalResources: List[Physical], humanResources: List[Human])


def newEndTime(start: Time, duration: Time): Time =
  start + duration

def getHumans(resources: List[PhysicalResourceType], humans: List[Human]) : List[Human] = {
  for {
    r <- resources
  } yield Human.getHumanByResource(r, humans)
}

def getPhysicals(resources: List[PhysicalResourceType], physicals: List[Physical]) : List[Physical] = {
  for {
    r <- resources
  } yield Physical.getPhysicalByResource(r, physicals)
}

def runTasks(productTasks: List[Task], pName: String ,order: Order , start: Time, end: Time,physicalResources: List[Physical],
               tasks: List[Task],
               humanResources: List[Human],
               products: List[Product],
               orders: List[Order]): List[ScheduleTask] = {
  for{
    t <- productTasks
  } yield ScheduleTask(order.id, pName, t.id, start, newEndTime(end, t.time), getPhysicals(t.resources,physicalResources), getHumans(t.resources,humanResources))
}

def runOrders(physicalResources: List[Physical],
              tasks: List[Task],
              humanResources: List[Human],
              products: List[domain.Product],
              orders: List[Order]): List[ScheduleTask] = {
  for{
    order <- orders
    p <- for {
      n <- (1 to order.quantity.toString.toInt).map(x => x).toList
      result <-
        val product = Product.getProduct(order.prdRef, products)
        val productTasks = Task.getTasksByProduct(product, tasks)
        runTasks(productTasks, n.toString, order, Time.zero, Time.zero, physicalResources, tasks, humanResources, products, orders)
    } yield result
  } yield p
}

def toScheduleTask(schedules: List[ScheduleTask]): List[Node] = {
  for {
    s <- schedules
    result <- <TaskSchedule order={s.order.toString} productNumber={s.productNumber.toString} task={s.task.toString} start={s.start.toString} end={s.end.toString}>
      <PhysicalResources>
        {s.physicalResources.map((physical)=>Physical.physicalXml(physical))}
      </PhysicalResources>
      <HumanResources>
        {s.humanResources.map((human)=> Human.humanXML(human))}
      </HumanResources>
    </TaskSchedule>
  } yield result
}

def toXML(schedules: List[ScheduleTask]): Result[Elem] = {
  val r = <Schedule xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../schedule.xsd ">
    {toScheduleTask(schedules)}
  </Schedule>
  if(schedules.isEmpty) Left(XMLError("")) else Right (r)
}

def create(n: Elem): Result[Elem] =
  for {
    physicalResources <- traverse( (n \\ "Physical"), Physical.from )
    tasks <- traverse( (n \\ "Task"), Task.from(physicalResources) )
    humanResources <- traverse( (n \\ "Human"), Human.from(physicalResources) )
    products <- traverse( (n \\ "Product"), domain.Product.from(tasks) )
    orders <- traverse( (n \\ "Order"), Order.from(products) )
    t <- Human.validateHumans(tasks,humanResources)
    result <- toXML(runOrders(physicalResources, tasks, humanResources, products,orders))
  } yield result

val a1 = <Production xmlns="http://www.dei.isep.ipp.pt/tap-2021" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/tap-2021 ../../production.xsd ">
  <PhysicalResources>
    <Physical id="PRS_1" type="PRST 1"/>
    <Physical id="PRS_2" type="PRST 2"/>
    <Physical id="PRS_2" type="PRST 2"/>
    <Physical id="PRS_3" type="PRST 3"/>
    <Physical id="PRS_4" type="PRST 4"/>
    <Physical id="PRS_5" type="PRST 5"/>
  </PhysicalResources>
  <Tasks>
    <Task id="TSK_1" time="100">
      <PhysicalResource type="PRST 1"/>
      <PhysicalResource type="PRST 4"/>
    </Task>
    <Task id="TSK_2" time="80">
      <PhysicalResource type="PRST 2"/>
      <PhysicalResource type="PRST 5"/>
    </Task>
  </Tasks>
  <HumanResources>
    <Human id="HRS_1" name="Antonio">
      <Handles type="PRST 1"/>
      <Handles type="PRST 2"/>
    </Human>
    <Human id="HRS_2" name="Maria">
      <Handles type="PRST 3"/>
      <Handles type="PRST 4"/>
      <Handles type="PRST 5"/>
    </Human>
  </HumanResources>
  <Products>
    <Product id="PRD_1" name="Product 1">
      <Process tskref="TSK_1"/>
      <Process tskref="TSK_2"/>
    </Product>
    <Product id="PRD_2" name="Product 2">
      <Process tskref="TSK_2"/>
    </Product>
  </Products>
  <Orders>
    <Order id="ORD_1" prdref="PRD_1" quantity="1"/>
    <Order id="ORD_2" prdref="PRD_2" quantity="2"/>
  </Orders>
</Production>

print(create(a1))